<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'DatabaseSeeder' => $baseDir . '/database/seeds/DatabaseSeeder.php',
    'ExampleTest' => $baseDir . '/tests/ExampleTest.php',
    'IlluminateQueueClosure' => $vendorDir . '/illuminate/queue/IlluminateQueueClosure.php',
    'TestCase' => $baseDir . '/tests/TestCase.php',
);
