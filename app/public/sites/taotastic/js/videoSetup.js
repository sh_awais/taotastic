taotastic.site.setup();/*
 * Rodeo Product Search
 * Usage:
 * respond.productSearch.setup();
 */
taotastic.videoSetup = (function () {
  'use strict';

  return {
    version: '0.0.1',
    setup: function () {
      var video, x;
      // GET

      video = $('#video-plugin:first')[0];
      taotastic.videoSetup.setupVideo(video);
    },
    /**
     * Setup function
     *
     */


    setupVideo: function (el) {
      var url, width, height, loop, autoplay, controls, mute;
      url = el.getAttribute('url');
      width = el.getAttribute('width');
      height = el.getAttribute('height');
      loop = el.getAttribute('loop');
      autoplay = el.getAttribute('auto');
      mute = el.getAttribute('mute');
      controls = el.getAttribute('controls');
      if (!url) {
        alert("Please enter URL in the plugins settings first");
        return false;
      }
      if (!width) {
        width = 800;

      }
      if (!height) {
        height = 520;

      }
      if (!loop) {
        loop = "";
      }
      else if (loop == "Yes") {
        loop = "loop"
      }
      else if (loop == "No") {
        loop = "";
      }
      if (!autoplay) {
        autoplay = "";

      }
      else if (autoplay == "Yes") {
        autoplay = "autoplay";

      }
      else if (autoplay == "No") {
        autoplay = "";

      }
      if (!mute) {
        mute = "";
      }
      else if (mute == "Yes") {
        mute = "muted"
      }
      else if (mute == "No") {
        mute = "";
      }
      if (!controls) {
        controls = "controls"
      }
      else if (controls == "Yes") {
        controls = "controls"
      }
      else if (controls == "No") {
        controls = "";
      }


      taotastic.videoSetup.videoSet(el, url, width, height, loop, autoplay, controls, mute);
    }
    ,

    videoSet: function (el, url, width, height, loop, autoplay, controls, mute) {
      // $(div_id).html('');
      // $(div_id).html(response.html);
      // $(div_id).removeClass('hide');
      var html_code = '<video ' + mute + ' ' + loop + ' ' + autoplay + ' ' + ' width=\"' + width + '\" height=\"'
        + height + '\" ' + controls + ' src =\" ' + url + '\"  /> '
      $(el).find('#video_page').html("");
      $(el).find('#video_page').append(html_code);
      $(el).find('#video_page').removeClass('hide');
    }
    ,


  }

})();

taotastic.videoSetup.setup();
